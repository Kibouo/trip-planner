# TripPlanner
## About
Nowadays, complete plans for a vacation trip are acquired one of two ways:
    
1) *Travel agency:* agencies do all the dirty work for you. However, they are expensive and you are bound to their schedule.
2) *Pen & paper:* prepare to easily spend hours looking for the cheapest hotel, trying to fit multiple events into your short vacation, or just finding the fastest route as to be on time for check-in. Scratching out a mistake will make the process just even more frustrating.

TripPlanner aims ease the process of creating your personal planning. It provides an intuitive drag and drop system so you can re-arrange items. As well as integration with many data providers, such as A‌i‌r‌b‌n‌b‌, TripAdvisor, and Booking.com, to allow for huge flexibility in your planning.
