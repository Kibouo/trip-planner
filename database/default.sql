CREATE TABLE session (
    key Uuid PRIMARY KEY,
    deadline Timestamptz NOT NULL DEFAULT now(),
    data Jsonb NOT NULL
);

CREATE TABLE trip (
    id VARCHAR(8) PRIMARY KEY,
    start TEXT NOT NULL,
    "end" TEXT NOT NULL,
    path Jsonb NOT NULL,
    UNIQUE (start, "end", path)
);

-- Function which removes expired sessions, triggered on insertion of new sessions. 
CREATE FUNCTION delete_expired_sessions() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM session WHERE deadline < NOW();
  RETURN NEW;
END;
$$;
CREATE TRIGGER delete_expired_sessions_trigger
    AFTER INSERT ON session
    EXECUTE PROCEDURE delete_expired_sessions();
