use crate::{
    data::ext_api::area::{area_lookup, areas::Areas},
    utils::{cache::area_cache::AreaCache, error::TPError}
};
use rocket::State;
use rocket_contrib::json::Json;

#[get("/api/area?<a>")]
pub fn get(cache: State<AreaCache>, a: String) -> Result<Json<Areas>, TPError>
{
    area_lookup(&cache.inner(), &a).map(Json)
}
