use rocket::{
    http::uri::{Origin, Uri},
    Route
};
use std::collections::HashMap;

mod area;
mod trip;
mod trips;

pub struct Routes<'a>(HashMap<&'a str, Route>);

impl<'a> Routes<'a>
{
    pub fn build() -> Self
    {
        Self(
            [
                ("area_get", routes![area::get]),
                ("trip_get", routes![trip::get]),
                ("trip_post", routes![trip::post]),
                ("trips_get", routes![trips::get])
            ]
            .iter()
            .map(|(name, routes)| {
                (
                    *name,
                    routes
                        .get(0)
                        .unwrap_or_else(|| panic!("Failed to build route '{}'", name))
                        .clone()
                )
            })
            .collect()
        )
    }

    pub fn list(&self) -> Vec<Route> { self.0.values().cloned().collect() }

    pub fn get(&'a self, key: &str) -> &'a Route
    {
        self.0
            .get(key)
            .unwrap_or_else(|| panic!("Route '{}' does not exist.", key))
    }

    pub fn uri_of(&'a self, key: &str) -> &'a Origin { &self.get(key).uri }

    pub fn uri_with_query(&self, key: &str, query_params: &[(&str, &str)]) -> Origin
    {
        let uri = self.uri_of(key);

        let path = uri.path();
        let mut query = uri
            .query()
            .unwrap_or_else(|| panic!("Uri of route '{}' does not contain a query.", key))
            .to_owned();

        query_params
            .iter()
            .map(|(q, p)| (q, Uri::percent_encode(p)))
            .for_each(|(q, p)| {
                query = query.replace(&format!("<{}>", q), &format!("{}={}", q, p));
            });

        Origin::parse_owned(format!("{}?{}", path, query)).unwrap_or_else(|err| {
            panic!(
                "Failed to parse uri of route '{}' with queries filled out. {}",
                key, err
            )
        })
    }
}
