use super::Routes;
use crate::{
    data::database::{
        trip::{Trip, TripRealAreas},
        DbConn
    },
    traits::{insert_db::InsertDb, query_db::QueryDb},
    utils::{cache::area_cache::AreaCache, error::TPError}
};
use rocket::{response::status::Created, State};
use rocket_contrib::json::Json;
use std::convert::TryFrom;

#[get("/api/trip?<t>")]
pub fn get(db_conn: DbConn, t: String) -> Result<Json<Trip>, TPError>
{
    Trip::query_db(&db_conn, &t).map(Json)
}

#[post("/api/trip", format = "json", data = "<trip>")]
pub fn post(
    db_conn: DbConn,
    area_cache: State<AreaCache>,
    routes: State<Routes>,
    trip: Json<Trip>
) -> Result<Created<()>, TPError>
{
    TripRealAreas::try_from((trip.into_inner(), area_cache.inner()))?
        .insert_db(&db_conn)
        .map(|trip| {
            Created(
                routes
                    .uri_with_query("trip_get", &[("t", trip.id())])
                    .to_string(),
                None
            )
        })
}
