use crate::{
    data::database::{trip::trips::Trips, DbConn},
    traits::query_db::QueryDb,
    utils::error::TPError
};
use rocket_contrib::json::Json;

// TODO: add pagination/limit
#[get("/api/trips?<start>&<end>")]
pub fn get(db_conn: DbConn, start: String, end: String) -> Result<Json<Trips>, TPError>
{
    Trips::query_db(&db_conn, (&start, &end)).map(Json)
}
