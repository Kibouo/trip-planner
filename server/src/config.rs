use rocket::config::{Config, Environment, Value};
use std::collections::HashMap;

// TODO: move this to config once it is supported
// (https://github.com/SergioBenitez/Rocket/issues/852)
pub(super) fn load() -> Config
{
    let mut database_config = HashMap::new();
    let mut databases = HashMap::new();
    let db_url = format!(
        "postgres://{}:{}@{}:{}/{}",
        dotenv!("DB_USER"),
        dotenv!("DB_PASS"),
        dotenv!("DB_IP"),
        dotenv!("DB_PORT"),
        dotenv!("DB_NAME"),
    );
    database_config.insert("url", Value::from(db_url));
    databases.insert("development", Value::from(database_config));

    Config::build(Environment::Staging)
        .extra("databases", databases)
        .finalize()
        .unwrap()
}
