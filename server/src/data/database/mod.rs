pub mod session;
pub mod trip;

use rocket_contrib::databases::diesel;

#[database("development")]
pub struct DbConn(diesel::PgConnection);
