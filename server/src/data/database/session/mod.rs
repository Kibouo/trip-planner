mod schema;
pub mod session_data;
mod table;

use crate::{
    data::database::DbConn,
    rocket::outcome::IntoOutcome,
    traits::query_db::QueryDb,
    utils::{cache::session_cache::SessionCache, error::TPError}
};
use chrono::{DateTime, Duration, Utc};
use diesel::prelude::*;
use rocket::{
    http::{Cookie, Cookies, SameSite, Status},
    request::{FromRequest, Outcome as RequestOutcome, Request, State}
};
use schema::session::{dsl as session_schema, SqlType as SessionSqlType};
use session_data::SessionData;
use std::convert::TryInto;
use table::SessionTable;
use uuid::Uuid;

const SESSION_COOKIE_NAME: &str = "session";
pub const SESSION_LIFETIME_SEC: i64 = 2 * 60 * 60;

fn session_lifetime() -> Duration { Duration::seconds(SESSION_LIFETIME_SEC) }

#[derive(new)]
struct Session
{
    id:       Uuid,
    deadline: DateTime<Utc>,
    data:     SessionData
}

// TODO: make this a fairing (extern crate?)
// TODO: look at async db calls
impl Session
{
    fn create(
        cookies: &mut Cookies,
        db_conn: &DbConn,
        cache: &SessionCache
    ) -> Result<Self, TPError>
    {
        let id = Uuid::new_v4();
        let data = SessionData::default();

        cookies.add_private(
            // TODO: set ``.secure(true)`` with SSL
            Cookie::build(SESSION_COOKIE_NAME, format!("{}", id))
                .max_age(session_lifetime())
                .domain(dotenv!("DOMAIN_NAME"))
                .http_only(false)
                .same_site(SameSite::Lax)
                .secure(false)
                .finish()
        );
        let session = Session::new(id, Utc::now() + session_lifetime(), data);

        session.save(db_conn, cache)?;
        Ok(session)
    }

    fn touch(
        mut self,
        cookie: &mut Cookie<'static>,
        cookies: &mut Cookies,
        db_conn: &DbConn,
        cache: &SessionCache
    ) -> Result<Self, TPError>
    {
        cookie.set_max_age(session_lifetime());
        cookies.add_private(cookie.clone());

        self.deadline = Utc::now() + session_lifetime();
        self.save(db_conn, cache)?;

        Ok(self)
    }

    /// Save session in cache and DB.
    fn save(&self, db_conn: &DbConn, cache: &SessionCache) -> Result<(), TPError>
    {
        diesel::insert_into(session_schema::session)
            .values(&SessionTable::from(self))
            .on_conflict(session_schema::key)
            .do_update()
            .set(&SessionTable::from(self))
            .execute(&**db_conn)
            .map(|_| ())
            .map_err(TPError::from)?;

        cache.insert(&self.id, &self.data);

        Ok(())
    }

    /// Load session, given an ID, from cache (1st) or DB (fallback).
    fn load(id: Uuid, db_conn: &DbConn, cache: &SessionCache) -> Result<Self, TPError>
    {
        let cache_hit = cache
            .get(&id)
            .map(|data| Session::new(id, Utc::now(), data))
            .ok_or_else(|| TPError::CustomError(format!("No cache hit for {}", id)));

        cache_hit
            .or_else(|orig_error| Session::query_db(db_conn, &id).map_err(|e| orig_error.chain(e)))
    }
}

impl QueryDb<&Uuid, SessionSqlType> for Session
{
    type DbType = SessionTable;

    fn query_db(db_conn: &DbConn, id: &Uuid) -> Result<Self, TPError>
    {
        session_schema::session
            .filter(session_schema::key.eq(id))
            .first::<SessionTable>(&**db_conn)?
            .try_into()
    }
}

impl From<&Session> for SessionTable
{
    fn from(session: &Session) -> SessionTable
    {
        SessionTable::new(
            session.id,
            session.deadline,
            serde_json::to_value(session.data.clone()).unwrap_or_default()
        )
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for Session
{
    type Error = TPError;

    fn from_request(req: &'a Request<'r>) -> RequestOutcome<Session, TPError>
    {
        let mut cookies = req.cookies();
        let db_conn = req.guard::<DbConn>().map_failure(|(status, _)| {
            (
                status,
                TPError::CustomError("Failed to get db connection from guard.".to_owned())
            )
        })?;
        let cache = req
            .guard::<State<SessionCache>>()
            .map_failure(|(status, _)| {
                (
                    status,
                    TPError::CustomError("Failed to get session cache from guard.".to_owned())
                )
            })?;

        let mut session_cookie = cookies.get_private(SESSION_COOKIE_NAME);
        let session = session_cookie
            .as_mut()
            .and_then(|cookie| {
                // cookie exists & can be parsed
                cookie.value().parse::<Uuid>().ok().map(|id| (cookie, id))
            })
            .and_then(|(cookie, session_id)| {
                // cookie is a valid session
                Session::load(session_id, &db_conn, &cache)
                    .ok()
                    .map(|session| (cookie, session))
            })
            .and_then(|(cookie, session)| {
                // Touch existing session.
                //
                // This relies on expired sessions being removed from the db, otherwise these
                // old sessions will be revived.
                session.touch(cookie, &mut cookies, &db_conn, &cache).ok()
            });

        // failed to work with old session? -> new session
        let session = session
            .ok_or(())
            .or_else(|_| Session::create(&mut cookies, &db_conn, &cache));

        session.into_outcome(Status::InternalServerError)
    }
}
