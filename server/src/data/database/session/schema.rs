table! {
    session (key) {
        key -> Uuid,
        deadline -> Timestamptz,
        data -> Jsonb,
    }
}
