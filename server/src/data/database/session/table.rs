use super::{schema::session, session_data::SessionData, Session};
use crate::utils::error::TPError;
use chrono::{DateTime, Utc};
use std::convert::TryFrom;

#[derive(Queryable, Insertable, AsChangeset, new)]
#[table_name = "session"]
pub(super) struct SessionTable
{
    key:      ::uuid::Uuid,
    deadline: DateTime<Utc>,
    data:     ::serde_json::Value
}

impl TryFrom<SessionTable> for Session
{
    type Error = TPError;

    fn try_from(session_table: SessionTable) -> Result<Session, Self::Error>
    {
        Ok(Session::new(
            session_table.key,
            session_table.deadline,
            serde_json::from_value::<SessionData>(session_table.data).map_err(TPError::from)?
        ))
    }
}
