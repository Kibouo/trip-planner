pub mod path;
mod schema;
mod table;
pub mod trips;

use crate::{
    data::{
        database::DbConn,
        ext_api::area::{area_lookup, areas::BestMatch, Area}
    },
    diesel::{Insertable, RunQueryDsl},
    traits::{insert_db::InsertDb, query_db::QueryDb},
    utils::{
        cache::area_cache::AreaCache,
        error::TPError,
        id::{generator::IdGenerator, Id}
    }
};
use diesel::prelude::*;
use path::TripPath;
use schema::trip::{dsl as trip_schema, table as DieselTripTable, SqlType as TripSqlType};
use std::convert::TryFrom;
use table::TripTable;

#[derive(new, Clone)]
pub struct TripRealAreas
{
    id: Option<Id>,
    start: Area,
    end: Area,
    #[new(default)]
    _path: Option<TripPath>
}

impl TripRealAreas
{
    pub fn start(&self) -> &Area { &self.start }

    pub fn end(&self) -> &Area { &self.end }
}

impl InsertDb<DieselTripTable> for TripRealAreas
{
    type DbType = TripTable;

    fn insert_db(self, db_conn: &DbConn) -> Result<Self::DbType, TPError>
    {
        TripTable::from(self)
            .insert_into(DieselTripTable)
            .get_result(&**db_conn)
            .map_err(TPError::from)
    }
}

impl From<TripRealAreas> for TripTable
{
    fn from(trip: TripRealAreas) -> TripTable
    {
        let id = match trip.id {
            Some(id) => id,
            None => IdGenerator::default().random_id()
        };

        TripTable::new(
            id.inner().to_owned(),
            trip.start.name().to_lowercase(),
            trip.end.name().to_lowercase()
        )
    }
}

#[derive(Serialize, Deserialize, new)]
pub struct Trip
{
    id: Option<String>,
    start: String,
    end: String,
    #[new(default)]
    path: Option<TripPath>
}

impl QueryDb<&str, TripSqlType> for Trip
{
    type DbType = TripTable;

    fn query_db(db_conn: &DbConn, id: &str) -> Result<Self, TPError>
    {
        trip_schema::trip
            .find(id)
            .first::<TripTable>(&**db_conn)
            .map(Trip::from)
            .map_err(TPError::from)
    }
}

impl TryFrom<(Trip, &AreaCache)> for TripRealAreas
{
    type Error = TPError;

    fn try_from((trip, area_cache): (Trip, &AreaCache)) -> Result<TripRealAreas, Self::Error>
    {
        fn real_area_from_str(cache: &AreaCache, area: &str) -> Result<Area, TPError>
        {
            area_lookup(cache, area).and_then(|areas| areas.best_match().map(Area::clone))
        }

        let id = match trip.id {
            Some(id) => Some(Id::try_from(id)?),
            None => None
        };

        Ok(TripRealAreas::new(
            id,
            real_area_from_str(area_cache, &trip.start)?,
            real_area_from_str(area_cache, &trip.end)?
        ))
    }
}
