pub mod edge;
pub mod node;

use edge::Edge;
use node::Node;

pub type TripPath = Vec<(Node, Option<Edge>)>;
