table! {
    trip (id) {
        id -> VarChar,
        start -> Text,
        end -> Text,
        path -> Jsonb,
    }
}
