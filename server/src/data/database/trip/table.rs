use super::{schema::trip, Trip};
use diesel::{
    backend::Backend,
    deserialize::{FromSql, Result as DeserializeResult},
    pg::Pg,
    sql_types::{Jsonb, Record, Text, VarChar}
};

// TODO: store path in a better way (those come from other APIs, how to solve
// this without copying API data?)
#[derive(Queryable, Insertable, new)]
#[table_name = "trip"]
pub struct TripTable
{
    id: String,
    start: String,
    end: String,
    #[new(default)]
    path: ::serde_json::Value
}

impl TripTable
{
    pub fn id(&self) -> &str { &self.id }
}

impl From<TripTable> for Trip
{
    fn from(trip_table: TripTable) -> Trip
    {
        Trip::new(Some(trip_table.id), trip_table.start, trip_table.end)
    }
}

impl FromSql<(VarChar, Text, Text, Jsonb), Pg> for TripTable
where
    String: FromSql<Text, Pg> + FromSql<VarChar, Pg>,
    ::serde_json::Value: FromSql<Jsonb, Pg>
{
    fn from_sql(bytes: Option<&<Pg as Backend>::RawValue>) -> DeserializeResult<Self>
    {
        FromSql::<Record<(VarChar, Text, Text, Jsonb)>, Pg>::from_sql(bytes).map(
            |(id, start, end, path)| {
                TripTable {
                    id,
                    start,
                    end,
                    path
                }
            }
        )
    }
}
