use super::{
    schema::trip::{dsl as trip_schema, SqlType as TripSqlType},
    table::TripTable,
    Trip
};
use crate::{data::database::DbConn, traits::query_db::QueryDb, utils::error::TPError};
use diesel::{pg::types::sql_types::Array, prelude::*};

#[derive(Serialize)]
pub struct Trips(Vec<Trip>);

impl QueryDb<(&str, &str), Array<TripSqlType>> for Trips
{
    type DbType = Vec<TripTable>;

    fn query_db(db_conn: &DbConn, (start, end): (&str, &str)) -> Result<Self, TPError>
    {
        let start = start.to_lowercase();
        let end = end.to_lowercase();

        Ok(trip_schema::trip
            .filter(trip_schema::start.eq(start))
            .filter(trip_schema::end.eq(end))
            .load::<TripTable>(&**db_conn)?
            .into())
    }
}

impl From<Vec<TripTable>> for Trips
{
    fn from(tables: Vec<TripTable>) -> Self { Trips(tables.into_iter().map(Trip::from).collect()) }
}
