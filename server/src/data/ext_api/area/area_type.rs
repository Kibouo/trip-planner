use enum_iterator::IntoEnumIterator;

#[derive(IntoEnumIterator, Serialize, Deserialize, Clone, EnumString, Display, Debug)]
pub enum AreaType
{
    Country,
    State,
    Region,
    Province,
    District,
    County,
    Municipality,
    City,
    Town,
    Village,
    Continent,
    Archipelago,
    Island
}
