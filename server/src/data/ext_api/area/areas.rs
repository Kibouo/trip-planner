use super::{area_type::AreaType, recv::AreasJson, Area};
use crate::{traits::fetch::Fetch, utils::error::TPError};
use enum_iterator::IntoEnumIterator;
use reqwest::Url;

const LIMIT_EXT_RESULTS: u8 = 5;

pub type Areas = Vec<Area>;

pub trait BestMatch
where Self: std::ops::Deref<Target = [Area]>
{
    fn best_match(&self) -> Result<&Area, TPError>
    {
        self.get(0)
            .ok_or_else(|| TPError::CustomError("No areas present.".to_owned()))
    }
}
impl BestMatch for Areas {}

impl Fetch for Areas
{
    type RecvType = AreasJson;

    fn api_url(query: &str) -> Result<Url, TPError>
    {
        let options = {
            let mut options = AreaType::into_enum_iter()
                .map(|area_type| {
                    format!(
                        "osm_tag=place:{}",
                        area_type.to_string().to_ascii_lowercase()
                    )
                })
                .collect::<Vec<String>>();
            options.push(format!("limit={}", LIMIT_EXT_RESULTS));
            options.join("&")
        };

        let url_string = format!("https://photon.komoot.de/api/?q={}&{}", query, options);
        Url::parse(&url_string)
            .map_err(|e| TPError::from(e).chain(format!("Failed to parse URL {}.", url_string)))
    }
}
