#![allow(clippy::too_many_arguments)] // for ``new`` function

pub mod area_type;
pub mod areas;
mod recv;

use crate::{
    traits::fetch::Fetch,
    utils::{cache::area_cache::AreaCache, error::TPError}
};
use area_type::AreaType;
use areas::Areas;

pub fn area_lookup(cache: &AreaCache, query_area: &str) -> Result<Areas, TPError>
{
    // Ensure place names match despite of capitalisation. The end-user could have
    // typed in lower-case which means capitalised place-names would not be found.
    let query_area = query_area.to_lowercase();

    // check for cache
    let cache_hit = cache.get(&query_area).ok_or(());

    // API request on cache miss
    cache_hit.or_else(|_| {
        Areas::fetch(&query_area).map(|areas| {
            cache.insert(&query_area, &areas);
            areas
        })
    })
}

#[derive(Serialize, Deserialize, Clone, new, Debug)]
pub struct Area
{
    #[serde(rename = "type")]
    type_: AreaType,
    name: String,
    address: Option<String>,
    city: Option<String>,
    zip_code: Option<String>,
    state: Option<String>,
    country: Option<String>,
    coordinates: [f64; 2]
}

impl Area
{
    pub fn name(&self) -> &str { &self.name }
}
