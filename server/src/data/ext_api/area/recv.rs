use super::{area_type::AreaType, areas::Areas, Area};
use changecase::ChangeCase;

impl Into<Areas> for AreasJson
{
    fn into(self) -> Areas
    {
        self.features
            .into_iter()
            .filter_map(|feature| feature.into())
            .collect()
    }
}

impl Into<Option<Area>> for FeatureJson
{
    fn into(self) -> Option<Area>
    {
        use std::str::FromStr;

        let props = self.properties;
        let coords = self.geometry.coordinates;

        let house_nr = props.housenumber;
        Some(Area::new(
            AreaType::from_str(&props.osm_value.to_capitalized()).ok()?,
            props.name?,
            props.street.map(|street| {
                house_nr
                    .map(|nr| format!("{} {}", street, nr))
                    .unwrap_or(street)
            }),
            props.city,
            props.postcode,
            props.state,
            props.country,
            [*(coords.get(0)?), *(coords.get(1)?)]
        ))
    }
}

#[allow(dead_code)]
#[derive(Deserialize, Clone)]
pub struct AreasJson
{
    features: Vec<FeatureJson>,
    #[serde(rename = "type")]
    type_: String
}

#[allow(dead_code)]
#[derive(Deserialize, Clone)]
struct FeatureJson
{
    geometry: GeometryJson,
    #[serde(rename = "type")]
    type_: String,
    properties: PropertiesJson
}

#[allow(dead_code)]
#[derive(Deserialize, Clone)]
struct GeometryJson
{
    coordinates: Vec<f64>,
    #[serde(rename = "type")]
    type_: String
}

#[allow(dead_code)]
#[derive(Deserialize, Clone)]
struct PropertiesJson
{
    osm_id:      i64,
    osm_type:    String,
    extent:      Option<Vec<f64>>,
    country:     Option<String>,
    osm_key:     String,
    housenumber: Option<String>,
    city:        Option<String>,
    street:      Option<String>,
    osm_value:   String,
    postcode:    Option<String>,
    name:        Option<String>,
    state:       Option<String>
}
