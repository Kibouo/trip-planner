#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate derive_new;
#[macro_use]
extern crate derive_more;
#[macro_use]
extern crate err_derive;
#[macro_use]
extern crate dotenv_codegen;
#[macro_use]
extern crate strum_macros;

mod api;
mod config;
mod data;
mod traits;
mod utils;

use crate::{
    api::Routes,
    data::database::DbConn,
    utils::cache::{area_cache::area_cache, session_cache::session_cache}
};

fn main()
{
    dotenv::dotenv().expect("Failed to load env vars.");

    let routes = Routes::build();
    rocket::custom(config::load())
        .attach(DbConn::fairing())
        .mount("/", routes.list())
        .manage(routes)
        .manage(area_cache())
        .manage(session_cache())
        .launch();
}
