use crate::utils::error::TPError;
use reqwest::Url;
use serde::de::DeserializeOwned;

pub trait Fetch: Sized
{
    type RecvType: DeserializeOwned + Into<Self>;

    fn api_url(query: &str) -> Result<Url, TPError>;

    fn fetch(query: &str) -> Result<Self, TPError>
    {
        Ok(reqwest::get(Self::api_url(query)?)?
            .json::<Self::RecvType>()
            .expect("Failed to parse API response. Did the external API change?")
            .into())
    }
}
