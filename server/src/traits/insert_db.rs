use crate::{data::database::DbConn, utils::error::TPError};
use diesel::Insertable;
use std::convert::TryInto;

pub trait InsertDb<T>: TryInto<<Self as InsertDb<T>>::DbType>
{
    type DbType: Insertable<T>;

    fn insert_db(self, db_conn: &DbConn) -> Result<Self::DbType, TPError>;
}
