use rocket::Outcome;
use std::fmt::Display;

// TODO: make logging async to prevent blockage.
trait Log<D>
where D: Display
{
    fn log(self, text: D) -> Self;
}

impl<T, E, D> Log<D> for Result<T, E>
where
    E: Display,
    D: Display
{
    fn log(self, text: D) -> Result<T, E>
    {
        if self.is_err() {
            eprintln!(
                "Error! {} {}",
                text,
                self.as_ref()
                    .err()
                    .expect("Failed to get error value from Result.")
            );
        }
        self
    }
}

impl<T, D> Log<D> for Option<T>
where D: Display
{
    fn log(self, text: D) -> Option<T>
    {
        if self.is_none() {
            eprintln!("Error! {}", text);
        }
        self
    }
}

impl<S, E, F, D> Log<D> for Outcome<S, E, F>
where D: Display
{
    fn log(self, text: D) -> Outcome<S, E, F>
    {
        if self.is_failure() {
            eprintln!("Error! {} {}", text, self);
        }
        self
    }
}
