// TODO: do we still need logging on application level? Result::Err is already
// logged by rocket when returning it from an endpoint.
// mod log;
pub mod fetch;
pub mod insert_db;
pub mod query_db;
