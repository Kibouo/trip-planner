use crate::{data::database::DbConn, utils::error::TPError};
use diesel::{pg::Pg, Queryable};
use std::convert::TryInto;

pub trait QueryDb<Q, ST>: Sized
{
    type DbType: Queryable<ST, Pg> + TryInto<Self>;

    // TODO: look at generic array for ``query`` param.
    fn query_db(db_conn: &DbConn, query: Q) -> Result<Self, TPError>;
}
