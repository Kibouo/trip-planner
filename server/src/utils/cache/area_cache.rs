use super::{Cache, CACHE_LINES};
use crate::data::ext_api::area::areas::Areas;

const ONE_DAY_IN_SECS: u64 = 24 * 60 * 60;

pub type AreaCache = Cache<String, Areas>;

pub fn area_cache() -> AreaCache { AreaCache::new(ONE_DAY_IN_SECS, CACHE_LINES) }
