pub mod area_cache;
pub mod session_cache;

use lru_time_cache::LruCache;
use std::{sync::Mutex, time::Duration};

const CACHE_LINES: u64 = 1024;

pub struct Cache<K, V>(Mutex<LruCache<K, V>>);

impl<K, V> Cache<K, V>
where
    K: Ord + Clone,
    V: Clone
{
    fn new(ttl: u64, lines: u64) -> Cache<K, V>
    {
        Cache(Mutex::new(
            LruCache::<K, V>::with_expiry_duration_and_capacity(
                Duration::from_secs(ttl),
                lines as usize
            )
        ))
    }

    pub fn get(&self, key: &K) -> Option<V>
    {
        self.0
            .lock()
            .expect("Failed to get lock on cache during get request.")
            .get(key)
            .cloned()
    }

    pub fn insert(&self, key: &K, value: &V)
    {
        self.0
            .lock()
            .expect("Failed to get lock on cache during insert request.")
            .insert(key.clone(), value.clone());
    }
}
