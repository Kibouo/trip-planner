use super::{Cache, CACHE_LINES};
use crate::data::database::session::{session_data::SessionData, SESSION_LIFETIME_SEC};
use uuid::Uuid;

pub type SessionCache = Cache<Uuid, SessionData>;

pub fn session_cache() -> SessionCache
{
    SessionCache::new(SESSION_LIFETIME_SEC as u64, CACHE_LINES)
}
