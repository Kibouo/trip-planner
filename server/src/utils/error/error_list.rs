use super::TPError;
use std::fmt::Display;

#[derive(Debug)]
pub struct ErrorList(Vec<TPError>);

impl Display for ErrorList
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result { write!(f, "{:?}", self.0) }
}

impl ErrorList
{
    pub fn new(item: TPError) -> Self { Self(vec![item]) }

    pub fn push(mut self, item: TPError) -> Self
    {
        self.0.push(item);
        self
    }
}
