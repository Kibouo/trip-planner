mod error_list;

use error_list::ErrorList;

#[derive(Error, Display, Debug, From)]
pub enum TPError
{
    CustomError(String),
    ErrorChain(ErrorList),
    DieselError(diesel::result::Error),
    SerdeError(serde_json::error::Error),
    UuidParseError(uuid::parser::ParseError),
    ReqwestError(reqwest::Error),
    ReqwestUrlError(reqwest::UrlError)
}

impl TPError
{
    pub fn chain(self, chained: impl Into<TPError>) -> TPError
    {
        let chained = chained.into();

        let new_error_list = match self {
            TPError::ErrorChain(error_list) => error_list.push(chained),
            _ => ErrorList::new(chained)
        };

        TPError::ErrorChain(new_error_list)
    }
}
