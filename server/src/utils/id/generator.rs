use super::{nibble::Nibble, Id, ID_LEN};
use base_custom::BaseCustom;
use rand::random;

// First item (nr 32) is _space_. This isn't useful in IDs so we skip it.
const START_LATIN_SCRIPT_UNICODE: u8 = 32 + 1;

pub struct IdGenerator
{
    id_len:         u8,
    max_value:      u64,
    base_converter: BaseCustom<char>
}

impl IdGenerator
{
    pub fn new(id_len: impl Into<Nibble>, base: impl Into<Nibble>) -> Self
    {
        let id_len = id_len.into();
        let base = base.into();

        let chars = (0..=*base)
            .map(|i: u8| (i + START_LATIN_SCRIPT_UNICODE) as char)
            .collect();

        Self {
            id_len:         *id_len,
            max_value:      u64::from(*base).pow((*id_len).into()),
            base_converter: BaseCustom::<char>::new(chars)
        }
    }

    pub fn from_base(base: impl Into<Nibble>) -> Self { Self::new(ID_LEN, base) }

    pub fn random_id(&self) -> Id
    {
        // pad with _space_ and replace as fmt doesn't allow parameterized padding chars
        let id = format!(
            "{:width$}",
            self.base_converter.gen(random::<u64>() % self.max_value),
            width = self.id_len as usize
        )
        .replace(" ", &self.base_converter.zero().to_string());

        Id::new_unchecked(id)
    }

    pub fn could_create(&self, id: &str) -> bool
    {
        self.base_converter.decimal(id) < self.max_value
    }
}

impl Default for IdGenerator
{
    fn default() -> Self { Self::from_base(16) }
}
