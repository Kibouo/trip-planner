pub mod generator;
mod nibble;

use crate::utils::error::TPError;
use generator::IdGenerator;
use std::convert::TryFrom;

const ID_LEN: u8 = 8;

#[derive(Clone)]
pub struct Id(String);

impl Id
{
    pub fn inner(&self) -> &str { &self.0 }

    /// Private constructor only to be used by the generator.
    fn new_unchecked(s: String) -> Self { Self(s) }
}

impl TryFrom<String> for Id
{
    type Error = TPError;

    fn try_from(s: String) -> Result<Self, Self::Error>
    {
        if s.len() == ID_LEN as usize && IdGenerator::default().could_create(&s) {
            Ok(Id(s))
        }
        else {
            Err(TPError::CustomError(format!(
                "Id '{}' is not a valid Id according to the defaults.",
                s
            )))
        }
    }
}
