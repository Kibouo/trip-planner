use std::ops::Deref;

#[allow(dead_code)]
const MIN: u8 = 0;
pub const MAX: u8 = 15;

/// Runtime checked nibble.
pub struct Nibble(u8);

impl From<u8> for Nibble
{
    fn from(val: u8) -> Self
    {
        if val <= MAX {
            Nibble(val)
        }
        else {
            panic!("Failed to store {} in Nibble.", val);
        }
    }
}

impl Deref for Nibble
{
    type Target = u8;

    fn deref(&self) -> &Self::Target { &self.0 }
}
